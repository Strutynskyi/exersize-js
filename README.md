Tasks: 
 1. Clone project;
 2.Create branch from "main" call it meaningful;
 3.Create application "To do list" with title "My TODOs".
 2. Below the title there should be input field where user can write his todos and button save;
 3. When user hit save button , input must be reflected below as 200px x 200px block with title (user entry to the form) and status 
 4. Todo blocs bust appear as a raw one by one
 5. Status of to do is Incomplete by default
 6. Every block has checkbox. If user clicks on checkbox status of Todo will be changed to completed
Use only html css and js for the project
Do not use any frameworks
Do not ask how to do things. Make your own research come up with the solution, then we will discuss it;
The words "I dont know how to do it" will automatically end this exercise 
How to style the app is up to you
When you done with part of app (eg title, or creating structure) make commits and merge request to main branch;
DO NOT FORGET to create separate branch for each task and make merge request;
I will review your code and will write comments what to fix
If you dont know how to work with git - READ IT dont ask
I'm NOT YOUR TEACHER! I can support your desire to learn and practice, but I can not learn instead of you. 
Asking and receiving ready answers is way to nowhere, so if you will do it I will not burn my time for it